//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>
#include "Array.hpp"
#include "LinkedList.hpp"
#include "Number.h"


bool testArray();

int main ()
{
    Array<int> array;
    Array<int> array2;
    
    
    if (testArray() == true)
    {
        std::cout << "Array tests passed\n";
    }
    else
    {
        std::cout << "Array tests failed\n";
    }
    
    
    if (array == array2)
    {
        std::cout << "they are the same\n";
        
    }
    
    else
    {
        std::cout << "they are not the same\n";
    }
    
    return 0;
}

bool testArray()
{
    Array<int> array;

    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof(testArray) / sizeof(testArray[0]);

    if (array.size() !=0)
    {
        std::cout << "size is wrong \n";
        return false;
    }

    for (int i =0; i < testArraySize;  i++)
    {
        array.add(testArray[i]);

        if (array.size() != i+1)
        {
            std::cout << "size is wrong \n";
            return false;
        }

        if (array.get(i) != testArray[i])
        {
            std::cout << "value at index: " << i << "recalled incorrectly\n";
            return false;
        }

        
    }
    
    return true;
}

