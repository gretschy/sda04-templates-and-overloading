//
//  LinkedList.hpp
//  CommandLineTool
//
//  Created by Tom on 09/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef LinkedList_hpp
#define LinkedList_hpp

#include <stdio.h>

class LinkedList
{
public:
    LinkedList();           /**constructor*/
    ~LinkedList();          /**destructor*/
    
    void add(float addData);            /**mutator which adds new items to the end of the array*/
    float get(int index) const;        /**accessor which returns the value stored in the Node at the specified index*/
    int size() const;                  /**accessor which returns the number of items currently in the linked list*/
    void clear();                       /**accessor which deletes all the nodes in the linked list**/

    struct Node
    {
        float data;
        Node* next;
    };
    
private:
    Node* head;
};

#endif /* LinkedList_hpp */
