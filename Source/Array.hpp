//
//  Array.hpp
//  CommandLineTool
//
//  Created by Tom on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef Array_hpp
#define Array_hpp

#include <stdio.h>
#include <iostream>

template <class Type>
class Array
{
public:
    
    Array()
    {
        arraySize = 0;
        arrayPtr = nullptr;
    }
    
    ~Array()
    {
        if (arrayPtr != nullptr)
        {
            delete [] arrayPtr;
        }
    }
    
    void add(Type ItemValue)
    {
        arraySize++;
        Type * tempPtr = new Type[arraySize];
        
        for (int i = 0; i < arraySize -1; i++)
        {
            tempPtr[i] = arrayPtr[i];
        }
        
        if (arrayPtr != nullptr)
        {
            delete [] arrayPtr;
        }

        tempPtr[arraySize - 1] = ItemValue;
        
        arrayPtr = tempPtr;
    }
    
    Type get(int index) const
    {
        if (index >= 0 && index < size())
        {
            return arrayPtr [index];
        }
        
        else
        {
            return 0;
        }
    }
    
    Type size() const
    {
        Type numberOfItems = arraySize;
        return numberOfItems;
    }
    
    /*operator overloading**/
    bool operator == (const Array& otherArray)
    {
        if (get() == otherArray.get())
        {
            if (size() == otherArray.size())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        
    }
    
    
    
private:
    int arraySize;
    Type *arrayPtr;
};




#endif /* Array_hpp */
